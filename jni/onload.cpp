#include "JNIHelp.h"
#include "jni.h"

namespace softwinner{

#define LOCAL_TAG "onload"

JavaVM *g_Jvm;

extern int register_DLNAManager(JNIEnv *env);
extern int register_DMC(JNIEnv *env);
extern int register_DMS(JNIEnv *env);

JNIEnv* AttachJavaThread(bool &attach)
{
    JNIEnv* env;
    g_Jvm->GetEnv((void**) &env, JNI_VERSION_1_4);
    attach = env ? false : true;
    if(attach)
        g_Jvm->AttachCurrentThread(&env, 0);
    return env;
}
void DetachJavaThread(bool attach)
{
    if(attach)
    {
        g_Jvm->DetachCurrentThread();
    }
}

extern "C" jint JNI_OnLoad(JavaVM* vm, void* reserved) {

	JNIEnv* env = NULL;
	jint result = -1;

	if (vm->GetEnv((void**) &env, JNI_VERSION_1_4) != JNI_OK) {
		LOGE("GetEnv failed!");
		return result;
	}

	g_Jvm = vm;

	if( register_DLNAManager(env) < 0 )
	{
		LOGE("register DLNAManager failed!");
		return result;
	}

	if( register_DMS(env) < 0 )
	{
		LOGE("register DMS failed!");
		return result;
	}

	if( register_DMC(env) < 0 )
	{
		LOGE("register DMC failed!");
		return result;
	}

	return JNI_VERSION_1_4;
}

}