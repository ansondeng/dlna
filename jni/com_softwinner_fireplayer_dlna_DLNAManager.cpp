#include "JNIHelp.h"
#include "jni.h"
#include "Platinum.h"

namespace softwinner
{
#define LOCAL_TAG "nativeDLNAManager"

extern JavaVM *g_Jvm;
extern JNIEnv* AttachJavaThread(bool &attach);
extern void DetachJavaThread(bool attach);

struct fields_t 
{
    jfieldID   mNativeInstance;

    jfieldID	nativeDMS;
    jfieldID	nativeDMC;
    // jfieldID	nativeDMR;
    // jfieldID	nativeDMB;
};
static struct fields_t fields;

static const char* const kClassPathName = "com/softwinner/fireplayer/dlna/DLNAManager";

static void registerField(JNIEnv *env)
{
	jclass clazz = env->FindClass(kClassPathName);
	fields.mNativeInstance = env->GetFieldID(clazz, "mNativeInstance", "I");
    if (fields.mNativeInstance == NULL)
    {
    	LOGE("can't find field mNativeInstance ");
    }

    clazz =  env->FindClass("com/softwinner/fireplayer/dlna/DMS");
    fields.nativeDMS = env->GetFieldID(clazz, "mNativeInstance", "I");
    if (fields.nativeDMS == NULL)
    {
    	LOGE("can't find field nativeDMS ");
    }

    clazz =  env->FindClass("com/softwinner/fireplayer/dlna/DMC");
    fields.nativeDMC = env->GetFieldID(clazz, "mNativeInstance", "I");
    if (fields.nativeDMC == NULL)
    {
    	LOGE("can't find field nativeDMC ");
    }
}

static jint DLNAManager_init(JNIEnv *env, jobject thiz)
{
	registerField(env);

	PLT_UPnP *manager = new PLT_UPnP();
	if( manager == NULL )
	{
		LOGE("DLNAManager_init, manager is null");
		return 0;
	}
	manager->Start(); 
	return (jint)manager;
}

static void DLNAManager_finalize(JNIEnv *env, jobject thiz)
{
	PLT_UPnP *manager = (PLT_UPnP *)env->GetIntField(thiz, fields.mNativeInstance);
	if( manager == NULL )
	{
		LOGE("DLNAManager_finalize, error on get manager");
		return;
	}
	manager->Stop();
	delete manager;
}

static void DLNAManager_AddDMS(JNIEnv *env,jobject thiz,jobject jdevice)
{
	LOGD("DLNAManager_AddDMS");
	PLT_UPnP *manager = (PLT_UPnP *)env->GetIntField(thiz, fields.mNativeInstance);
	PLT_FileMediaServer *dms = (PLT_FileMediaServer*)env->GetIntField(jdevice, fields.nativeDMS);
	if( manager == NULL || dms == NULL )
	{
		LOGE("DLNAManager_AddDMS, error on get manager or dms");
		return;
	}
	PLT_DeviceHostReference server(dms);
	manager->AddDevice(server);
}

static void DLNAManager_RemoveDMS(JNIEnv *env,jobject thiz,jobject jdevice)
{
	LOGD("DLNAManager_RemoveDMS");
	PLT_UPnP *manager = (PLT_UPnP *)env->GetIntField(thiz, fields.mNativeInstance);
	PLT_FileMediaServer *dms = (PLT_FileMediaServer*)env->GetIntField(jdevice, fields.nativeDMS);
	if( manager == NULL || dms == NULL )
	{
		LOGE("DLNAManager_RemoveDMS, error on get manager or dms");
		return;
	}
	PLT_DeviceHostReference server(dms);
	manager->RemoveDevice(server);
}

static void DLNAManager_AddDMC(JNIEnv *env,jobject thiz,jobject jcp)
{
	LOGD("DLNAManager_AddDMC");
	PLT_UPnP *manager = (PLT_UPnP *)env->GetIntField(thiz, fields.mNativeInstance);
	PLT_MediaController *dmc = (PLT_MediaController*)env->GetIntField(jcp, fields.nativeDMC);
	if( manager == NULL || dmc == NULL )
	{
		LOGE("DLNAManager_AddDMC, error on get manager or dmc");
		return;
	}
	PLT_CtrlPointReference cp = dmc->GetCtrlPoint();
	manager->AddCtrlPoint(cp);
}

static void DLNAManager_RemoveDMC(JNIEnv *env,jobject thiz,jobject jcp)
{
	LOGD("DLNAManager_RemoveDMC");
	PLT_UPnP *manager = (PLT_UPnP *)env->GetIntField(thiz, fields.mNativeInstance);
	PLT_MediaController *dmc = (PLT_MediaController*)env->GetIntField(jcp, fields.nativeDMC);
	if( manager == NULL || dmc == NULL )
	{
		LOGE("DLNAManager_RemoveDMC, error on get manager or dmc");
		return;
	}
	PLT_CtrlPointReference cp = dmc->GetCtrlPoint();
	manager->RemoveCtrlPoint(cp);
}

static JNINativeMethod gMethods[] = 
{
	{"nativeInit"              ,"()I",
	        (void*)DLNAManager_init},
	{"nativeFinalize"          ,"()V",
	        (void*)DLNAManager_finalize},
    {"nativeAddDMS"         ,"(Lcom/softwinner/fireplayer/dlna/DMS;)V",
    		(void*)DLNAManager_AddDMS},
	{"nativeRemoveDMS"      ,"(Lcom/softwinner/fireplayer/dlna/DMS;)V",
			(void*)DLNAManager_RemoveDMS},
	{"nativeAddDMC"        ,"(Lcom/softwinner/fireplayer/dlna/DMC;)V",
			(void*)DLNAManager_AddDMC},
	{"nativeRemoveDMC","(Lcom/softwinner/fireplayer/dlna/DMC;)V",
			(void*)DLNAManager_RemoveDMC},
};

int register_DLNAManager(JNIEnv *env)
{
	// NPT_LogManager::GetDefault().Configure(
	// 			"plist:.level=SEVERE;.handlers=ConsoleHandler;.ConsoleHandler.colors=off;.ConsoleHandler.filter=49;.forward=false|"
	// 			"file:/mnt/sdcard/logging.prop");
	
	return jniRegisterNativeMethods(env, kClassPathName, gMethods , NELEM(gMethods));
}

}