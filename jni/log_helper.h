#ifndef LIBDLNA_COMMON_H_
#define LIBDLNA_COMMON_H_

#include <android/log.h>

#ifndef LOGD
#define LOGD(...) ((void)__android_log_print(ANDROID_LOG_DEBUG, LOCAL_TAG, __VA_ARGS__))
#endif

#ifndef LOGI
#define LOGI(...) ((void)__android_log_print(ANDROID_LOG_INFO, LOCAL_TAG, __VA_ARGS__))
#endif

#ifndef LOGW
#define LOGW(...) ((void)__android_log_print(ANDROID_LOG_WARN, LOCAL_TAG, __VA_ARGS__))
#endif

#ifndef LOGE
#define LOGE(...) ((void)__android_log_print(ANDROID_LOG_ERROR, LOCAL_TAG, __VA_ARGS__))
#endif

#ifndef ALOGV
#define ALOGV(...) ((void)__android_log_print(ANDROID_LOG_DEBUG, LOCAL_TAG, __VA_ARGS__))
#endif

#ifndef ALOGD
#define ALOGD(...) ((void)__android_log_print(ANDROID_LOG_DEBUG, LOCAL_TAG, __VA_ARGS__))
#endif

#ifndef ALOGI
#define ALOGI(...) ((void)__android_log_print(ANDROID_LOG_INFO, LOCAL_TAG, __VA_ARGS__))
#endif

#ifndef ALOGW
#define LOGW(...) ((void)__android_log_print(ANDROID_LOG_WARN, LOCAL_TAG, __VA_ARGS__))
#endif

#ifndef ALOGE
#define LOGE(...) ((void)__android_log_print(ANDROID_LOG_ERROR, LOCAL_TAG, __VA_ARGS__))
#endif

#endif 