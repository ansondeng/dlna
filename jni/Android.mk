#WITH_MIRROR := Y

LOCAL_PATH:= $(call my-dir)

LS_CPP=$(subst $(1)/,,$(wildcard $(1)/$(2)/*.cpp))
LS_C=$(subst $(1)/,,$(wildcard $(1)/$(2)/*.c))

include $(CLEAR_VARS)

NEPTUNE_C_INCLUDES := $(LOCAL_PATH)/libplatinum/Neptune/Source/Core 
		 
PLATINUM_C_INCLUDES :=  $(LOCAL_PATH)/libplatinum/Platinum/Source/Core \
	$(LOCAL_PATH)/libplatinum/Platinum/Source/Platinum \
	$(LOCAL_PATH)/libplatinum/Platinum/Source/Devices/MediaConnect \
	$(LOCAL_PATH)/libplatinum/Platinum/Source/Devices/MediaRenderer \
	$(LOCAL_PATH)/libplatinum/Platinum/Source/Devices/MediaServer \
	$(LOCAL_PATH)/libplatinum/Platinum/Source/Extras/ \
	$(NEPTUNE_C_INCLUDES) 
		 
NATIVE_C_INCLUDES := $(PLATINUM_C_INCLUDES) 

APP_C_INCLUDES := $(NATIVE_C_INCLUDES) 

APP_SRC_FILES := com_softwinner_fireplayer_dlna_DLNAManager.cpp \
	com_softwinner_fireplayer_dlna_DMC.cpp \
	com_softwinner_fireplayer_dlna_DMS.cpp \
	JNIHelp.cpp

APP_SRC_FILES += onload.cpp
        
LOCAL_C_INCLUDES := $(APP_C_INCLUDES)
LOCAL_SRC_FILES := $(APP_SRC_FILES)

LOCAL_MODULE_TAGS := optional

LOCAL_MODULE := libdlna

LOCAL_LDLIBS := -llog -lstdc++
					
LOCAL_STATIC_LIBRARIES := libplatinum \
	libneptune \
	libaxTLS \
	libzlib

include $(BUILD_SHARED_LIBRARY)

include $(call all-makefiles-under,$(LOCAL_PATH))
