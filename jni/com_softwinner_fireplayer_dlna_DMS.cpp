#include "JNIHelp.h"
#include "jni.h"
#include "Platinum.h"
#include "log_helper.h"

namespace softwinner
{
#define LOCAL_TAG "nativeDMS"

extern JavaVM *g_Jvm;
extern JNIEnv* AttachJavaThread(bool &attach);
extern void DetachJavaThread(bool attach);

struct fields_t 
{
    jfieldID   mNativeInstance;
};
static struct fields_t fields;

static const char* const kClassPathName = "com/softwinner/fireplayer/dlna/DMS";

static void registerField(JNIEnv *env)
{
	jclass clazz = env->FindClass(kClassPathName);
	fields.mNativeInstance = env->GetFieldID(clazz, "mNativeInstance", "I");
    if (fields.mNativeInstance == NULL)
    {
    	LOGE("can't find field mNativeInstance ");
    }
}

static jint DMS_init(JNIEnv *env, jobject thiz, jstring root, jstring name)
{
	registerField(env);
	
	const char* _root = env->GetStringUTFChars(root, NULL);
	const char* _name = env->GetStringUTFChars(name, NULL);
	PLT_FileMediaServer *dms = new PLT_FileMediaServer(_root, _name);
	if( dms == NULL )
	{
		LOGE("DMS_init, dms is null");
		return 0;
	}
	env->ReleaseStringUTFChars(root, _root);
	env->ReleaseStringUTFChars(name, _name);
	return (jint)dms;
}

static void DMS_finalize(JNIEnv *env, jobject thiz)
{
	PLT_FileMediaServer *dms = (PLT_FileMediaServer*)env->GetIntField(thiz, fields.mNativeInstance);
	if( dms == NULL )
	{
		LOGE("DMS_finalize, error on get dms");
		return;
	}
	delete dms;
}
 
static jstring DMS_GetURLFromPath(JNIEnv *env, jobject thiz, jstring path)
{
	PLT_FileMediaServer *dms = (PLT_FileMediaServer*)env->GetIntField(thiz, fields.mNativeInstance);
	if( dms == NULL )
	{
		LOGE("DMS_GetURLFromPath, error on get dms");
		return NULL;
	}

	const char* _path = env->GetStringUTFChars(path, NULL);
	NPT_String url = dms->Path2Uri(_path);
	env->ReleaseStringUTFChars(path, _path);
	return env->NewStringUTF(url.GetChars() );
}

static JNINativeMethod gMethods[] = 
{
	{"nativeInit"              ,"(Ljava/lang/String;Ljava/lang/String;)I",
	        (void*)DMS_init},
	{"nativeFinalize"          ,"()V",
	        (void*)DMS_finalize},
	{"nativeGetURLFromPath"	   ,"(Ljava/lang/String;)Ljava/lang/String;",
			(void*)DMS_GetURLFromPath},
};

int register_DMS(JNIEnv *env)
{
	return jniRegisterNativeMethods(env, kClassPathName, gMethods , NELEM(gMethods));
}

}