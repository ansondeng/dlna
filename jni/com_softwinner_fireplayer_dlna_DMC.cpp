#include "JNIHelp.h"
#include "jni.h"
#include "Platinum.h"
#include "log_helper.h"

namespace softwinner
{
#define LOCAL_TAG "nativeDMC"

#define DLNA_STOPPED 0
#define DLNA_PAUSED_PLAYBACK 1
#define DLNA_PLAYING 2
#define DLNA_TRANSITIONING 3

#define DLNA_NO_MEDIA_PRESENT 4


extern JavaVM *g_Jvm;
extern JNIEnv* AttachJavaThread(bool &attach);
extern void DetachJavaThread(bool attach);

struct fields_t 
{
    jfieldID   mNativeInstance;
};
static struct fields_t fields;

struct methods_t
{
	jmethodID onMRAdded;
	jmethodID onMRRemoved;

	jmethodID onMRTransportStateChanged;
    jmethodID onMRVolumeChanged;

	// jmethodID onGetCurrentTransportActionsResult;
	// jmethodID onGetDeviceCapabilitiesResult;
	jmethodID onGetMediaInfoResult;
	jmethodID onGetPositionInfoResult;
	jmethodID onGetTransportInfoResult;
    jmethodID onSeekResult;
    jmethodID onSetAVTransportURIResult;

	// jmethodID onGetProtocolInfoResult;

	// jmethodID onSetMuteResult;
	jmethodID onGetMuteResult;
	// jmethodID onSetVolumeResult;
	jmethodID onGetVolumeResult;
};
static struct methods_t methods;

static const char* const kClassPathName = "com/softwinner/fireplayer/dlna/DMC";
static const char* const kInterfacePathName = "com/softwinner/fireplayer/dlna/DMC$onDMCListener";

static jobject javaDMC = NULL;

class DMCDelegate : public PLT_MediaControllerDelegate
{
public:
    DMCDelegate(jobject obj)
    {
        mDMC = obj;

        mStateMap["STOPPED"] = DLNA_STOPPED;
        mStateMap["PAUSED_PLAYBACK"] = DLNA_PAUSED_PLAYBACK;
        mStateMap["PLAYING"] = DLNA_PLAYING;
        mStateMap["TRANSITIONING"] = DLNA_TRANSITIONING;
        mStateMap["NO_MEDIA_PRESENT"] = DLNA_NO_MEDIA_PRESENT;
    }
    ~DMCDelegate() {}

    bool OnMRAdded(PLT_DeviceDataReference& dev)
    {
        LOGD("OnMRAdded");
        bool attach;
        JNIEnv *env = AttachJavaThread(attach);
        const char *uuid = dev->GetUUID().GetChars();
        const char *name = dev->GetFriendlyName().GetChars();
        jstring juuid = env->NewStringUTF(uuid);
        jstring jname = env->NewStringUTF(name);
        env->CallVoidMethod(mDMC, methods.onMRAdded, juuid, jname);
        env->DeleteLocalRef(jname);
        env->DeleteLocalRef(juuid);
        DetachJavaThread(attach);
        return true;
    }

    void OnMRRemoved(PLT_DeviceDataReference& dev)
    {
        LOGD("OnMRRemoved");
        bool attach;
        JNIEnv *env = AttachJavaThread(attach);
        const char *uuid = dev->GetUUID().GetChars();
        jstring juuid = env->NewStringUTF(uuid);
        env->CallVoidMethod(mDMC, methods.onMRRemoved, juuid);
        env->DeleteLocalRef(juuid);
        DetachJavaThread(attach);
    }

    static int StringState2NumState(NPT_String ss)
    {

    }
    void OnMRStateVariablesChanged(PLT_Service* service, NPT_List<PLT_StateVariable*>* vars) 
    {
        
        bool attach;
        JNIEnv *env = AttachJavaThread(attach);
        
        int size = vars->GetItemCount();
        LOGD("OnMRStateVariablesChanged, size %d", size);
        for (int i = 0; i < size; i++)
        {
            PLT_StateVariable* state = vars->GetItem(i).operator *();
            LOGD("state variable %s, value %s", state->GetName().GetChars(), state->GetValue().GetChars() ); 
            if (state->GetName().Compare("TransportState", true) == 0)
            {
                
                env->CallVoidMethod(mDMC, methods.onMRTransportStateChanged, mStateMap[state->GetValue()]);
            }
            else if(state->GetName().Compare("Volume", true) == 0)
            {
                int volume;
                state->GetValue().ToInteger(volume);
                env->CallVoidMethod(mDMC, methods.onMRVolumeChanged, volume);
            }
        }

        DetachJavaThread(attach);
    }

    void OnGetMediaInfoResult(
        NPT_Result               res/* res */,
        PLT_DeviceDataReference& device/* device */,
        PLT_MediaInfo*           info/* info */,
        void*                    userdata/* userdata */) 
    {
        bool attach;
        JNIEnv *env = AttachJavaThread(attach);

        LOGD("OnGetMediaInfoResult %p", (void*)info ); 
        if( info )
        {
            int dura = info->media_duration.ToSeconds();
            const char *uri = info->cur_uri.GetChars();
            const char *metadata = info->cur_metadata.GetChars();
            jstring juri = env->NewStringUTF(uri);
            jstring jmetadata = env->NewStringUTF(metadata);
            env->CallVoidMethod(mDMC, methods.onGetMediaInfoResult, dura, juri, jmetadata);
            env->DeleteLocalRef(jmetadata);
            env->DeleteLocalRef(juri);
        }

        DetachJavaThread(attach);
    }

    void OnGetPositionInfoResult(
        NPT_Result               res/* res */,
        PLT_DeviceDataReference& device/* device */,
        PLT_PositionInfo*        info/* info */,
        void*                    userdata/* userdata */) 
    {
        bool attach;
        JNIEnv *env = AttachJavaThread(attach);
        LOGD("OnGetPositionInfoResult %p", (void*)info ); 
        if( info )
        {
            int pos = info->rel_time.ToMillis();
            int dura = info->track_duration.ToMillis();
            // LOGD("pos %d, dura %d", pos, dura);
            env->CallVoidMethod(mDMC, methods.onGetPositionInfoResult, dura, pos);
        }
        DetachJavaThread(attach);
    }

    void OnGetTransportInfoResult(
        NPT_Result                res ,
        PLT_DeviceDataReference&  device ,
        PLT_TransportInfo*        info ,
        void*                     userdata ) 
    {
        bool attach;
        JNIEnv *env = AttachJavaThread(attach);

        LOGD("OnGetTransportInfoResult %p", (void*)info ); 
        if( info )
        {
            env->CallVoidMethod(mDMC, methods.onGetTransportInfoResult, mStateMap[info->cur_transport_state]);
        }

        DetachJavaThread(attach);
    }

    void OnSeekResult(
        NPT_Result                res ,
        PLT_DeviceDataReference&  device ,
        void*                     userdata ) 
    {
        bool attach;
        JNIEnv *env = AttachJavaThread(attach);

        env->CallVoidMethod(mDMC, methods.onSeekResult);

        DetachJavaThread(attach);
    }

    void OnSetAVTransportURIResult(
        NPT_Result                res ,
        PLT_DeviceDataReference&  device ,
        void*                     userdata ) 
    {
        bool attach;
        JNIEnv *env = AttachJavaThread(attach);

        env->CallVoidMethod(mDMC, methods.onSetAVTransportURIResult);

        DetachJavaThread(attach);
    }

    void OnGetMuteResult(
        NPT_Result                res ,
        PLT_DeviceDataReference&  device ,
        const char*               channel ,
        bool                      mute ,
        void*                     userdata  ) 
    {
        bool attach;
        JNIEnv *env = AttachJavaThread(attach);

        env->CallVoidMethod(mDMC, methods.onGetMuteResult, mute);

        DetachJavaThread(attach);
    }

    virtual void OnGetVolumeResult(
        NPT_Result                res ,
        PLT_DeviceDataReference&  device ,
        const char*               channel ,
        NPT_UInt32                volume ,
        void*                     userdata ) 
    {
        bool attach;
        JNIEnv *env = AttachJavaThread(attach);

        env->CallVoidMethod(mDMC, methods.onGetVolumeResult, volume);

        DetachJavaThread(attach);
    }

private:
    jobject mDMC;
    NPT_Map<NPT_String, int> mStateMap;
};

static void registerField(JNIEnv *env)
{
	jclass clazz = env->FindClass(kClassPathName);
	fields.mNativeInstance = env->GetFieldID(clazz, "mNativeInstance", "I");
    if (fields.mNativeInstance == NULL)
    {
    	LOGE("can't find field mNativeInstance ");
    }
}

static void registerMethods(JNIEnv *env)
{
	jclass clazz = env->FindClass(kClassPathName);

	methods.onMRAdded = env->GetMethodID(clazz, "onMRAdded", "(Ljava/lang/String;Ljava/lang/String;)V");
    if (methods.onMRAdded == NULL)
    {
    	LOGE("can't find method onMRAdded ");
    }
    methods.onMRRemoved = env->GetMethodID(clazz, "onMRRemoved", "(Ljava/lang/String;)V");
    if (methods.onMRRemoved == NULL)
    {
    	LOGE("can't find method onMRRemoved ");
    }

    methods.onMRTransportStateChanged = env->GetMethodID(clazz, "onMRTransportStateChanged", "(I)V");
    if (methods.onMRTransportStateChanged == NULL)
    {
    	LOGE("can't find method onMRTransportStateChanged ");
    }
    methods.onMRVolumeChanged = env->GetMethodID(clazz, "onMRVolumeChanged", "(I)V");
    if (methods.onMRVolumeChanged == NULL)
    {
        LOGE("can't find method onMRVolumeChanged ");
    }

    //AVTransport
    // methods.onGetCurrentTransportActionsResult = env->GetMethodID(clazz, "onGetCurrentTransportActionsResult", "I");
    // if (methods.onGetCurrentTransportActionsResult == NULL)
    // {
    // 	LOGE("can't find method onGetCurrentTransportActionsResult ");
    // }
    // methods.onGetDeviceCapabilitiesResult = env->GetMethodID(clazz, "onGetDeviceCapabilitiesResult", "I");
    // if (methods.onGetDeviceCapabilitiesResult == NULL)
    // {
    // 	LOGE("can't find method onGetDeviceCapabilitiesResult ");
    // }
    methods.onGetMediaInfoResult = env->GetMethodID(clazz, "onGetMediaInfoResult", "(ILjava/lang/String;Ljava/lang/String;)V");
    if (methods.onGetMediaInfoResult == NULL)
    {
    	LOGE("can't find method onGetMediaInfoResult ");
    }
    methods.onGetPositionInfoResult = env->GetMethodID(clazz, "onGetPositionInfoResult", "(II)V");
    if (methods.onGetPositionInfoResult == NULL)
    {
    	LOGE("can't find method onGetPositionInfoResult ");
    }
    methods.onGetTransportInfoResult = env->GetMethodID(clazz, "onGetTransportInfoResult", "(I)V");
    if (methods.onGetTransportInfoResult == NULL)
    {
    	LOGE("can't find method onGetTransportInfoResult ");
    }
    methods.onSeekResult = env->GetMethodID(clazz, "onSeekResult", "()V");
    if (methods.onSeekResult == NULL)
    {
        LOGE("can't find method onSeekResult");
    }
    methods.onSetAVTransportURIResult = env->GetMethodID(clazz, "onSetAVTransportURIResult", "()V");
    if (methods.onSetAVTransportURIResult == NULL)
    {
        LOGE("can't find method onSetAVTransportURIResult ");
    }

    //ConnectionManager
    // methods.onGetProtocolInfoResult = env->GetMethodID(clazz, "onGetProtocolInfoResult", "I");
    // if (methods.onGetProtocolInfoResult == NULL)
    // {
    // 	LOGE("can't find method onGetProtocolInfoResult ");
    // }

    //RenderingControl
    // methods.onSetMuteResult = env->GetMethodID(clazz, "onSetMuteResult", "I");
    // if (methods.onSetMuteResult == NULL)
    // {
    // 	LOGE("can't find method onSetMuteResult ");
    // }
    methods.onGetMuteResult = env->GetMethodID(clazz, "onGetMuteResult", "(Z)V");
    if (methods.onGetMuteResult == NULL)
    {
    	LOGE("can't find method onGetMuteResult ");
    }
    // methods.onSetVolumeResult = env->GetMethodID(clazz, "onSetVolumeResult", "I");
    // if (methods.onSetVolumeResult == NULL)
    // {
    // 	LOGE("can't find method onSetVolumeResult ");
    // }
    methods.onGetVolumeResult = env->GetMethodID(clazz, "onGetVolumeResult", "(I)V");
    if (methods.onGetVolumeResult == NULL)
    {
    	LOGE("can't find method onGetVolumeResult ");
    }
}

static jint DMC_init(JNIEnv *env, jobject thiz)
{
	registerField(env);
	registerMethods(env);

    javaDMC = env->NewGlobalRef(thiz);
	PLT_CtrlPointReference ctrlPoint(new PLT_CtrlPoint());
	PLT_MediaController *dmc = new PLT_MediaController(ctrlPoint);
    DMCDelegate *delegate = new DMCDelegate(javaDMC);

    if( dmc==NULL || delegate==NULL )
    {
        LOGE("DMC_init, dmc or DMC_init is null");
        return 0;
    }    
    dmc->SetDelegate(delegate);
	return (jint)dmc;
}

static void DMC_finalize(JNIEnv *env, jobject thiz)
{
    if(!javaDMC)
    {
        env->DeleteGlobalRef(javaDMC);
        javaDMC = NULL;
    }

	PLT_MediaController *dmc = (PLT_MediaController*)env->GetIntField(thiz, fields.mNativeInstance);
    if( dmc == NULL )
    {
        LOGE("DMC_finalize, error on get dmc");
        return;
    }
    delete (DMCDelegate*)dmc->GetDelegate();
	delete dmc;
}

static void DMC_Search(JNIEnv *env, jobject thiz)
{
    LOGD("DMC_Search");
    PLT_MediaController *dmc = (PLT_MediaController*)env->GetIntField(thiz, fields.mNativeInstance);
    if( dmc == NULL )
    {
        LOGE("DMC_Search, error on get dmc");
        return;
    }
    dmc->GetCtrlPoint()->Search();
}

static PLT_DeviceDataReference uuid2DeviceData(JNIEnv *env, PLT_MediaController *dmc, jstring uuid)
{
    const char* _uuid = env->GetStringUTFChars(uuid, NULL);
    PLT_DeviceDataReference dev;
    dmc->FindRenderer(_uuid, dev);
    env->ReleaseStringUTFChars(uuid, _uuid);
    if( dev.IsNull() )
    {
        LOGE("uuid2DeviceData, find no device");
    }
    return dev;
}

static void DMC_GetMediaInfo(JNIEnv *env, jobject thiz, jstring uuid)
{
    LOGD("DMC_GetMediaInfo");
    PLT_MediaController *dmc = (PLT_MediaController*)env->GetIntField(thiz, fields.mNativeInstance);
    if( dmc == NULL )
    {
        LOGE("DMC_GetMediaInfo, error on get dmc");
        return;
    }
    PLT_DeviceDataReference dev = uuid2DeviceData(env, dmc, uuid);       
    dmc->GetMediaInfo(dev, 0, NULL);
}

static void DMC_GetPositionInfo(JNIEnv *env, jobject thiz, jstring uuid)
{
    LOGD("DMC_GetPositionInfo");
    PLT_MediaController *dmc = (PLT_MediaController*)env->GetIntField(thiz, fields.mNativeInstance);
    if( dmc == NULL )
    {
        LOGE("DMC_GetPositionInfo, error on get dmc");
        return;
    }
    PLT_DeviceDataReference dev = uuid2DeviceData(env, dmc, uuid);   
    dmc->GetPositionInfo(dev, 0, NULL);
}

static void DMC_GetTransportInfo(JNIEnv *env, jobject thiz, jstring uuid)
{
    LOGD("DMC_GetTransportInfo");
    PLT_MediaController *dmc = (PLT_MediaController*)env->GetIntField(thiz, fields.mNativeInstance);
    if( dmc == NULL )
    {
        LOGE("DMC_GetTransportInfo, error on get dmc");
        return;
    }
    PLT_DeviceDataReference dev = uuid2DeviceData(env, dmc, uuid);   
    dmc->GetTransportInfo(dev, 0, NULL);
}

static void DMC_Pause(JNIEnv *env, jobject thiz, jstring uuid)
{
    LOGD("DMC_Pause");
    PLT_MediaController *dmc = (PLT_MediaController*)env->GetIntField(thiz, fields.mNativeInstance);
    if( dmc == NULL )
    {
        LOGE("DMC_Pause, error on get dmc");
        return;
    }
    PLT_DeviceDataReference dev = uuid2DeviceData(env, dmc, uuid);     
    dmc->Pause(dev, 0, NULL);
}

static void DMC_Play(JNIEnv *env, jobject thiz, jstring uuid)
{
    LOGD("DMC_Play");
    PLT_MediaController *dmc = (PLT_MediaController*)env->GetIntField(thiz, fields.mNativeInstance);
    if( dmc == NULL )
    {
        LOGE("DMC_Play, error on get dmc");
        return;
    }
    PLT_DeviceDataReference dev = uuid2DeviceData(env, dmc, uuid);      
    dmc->Play(dev, 0, "1", NULL);
}

static void DMC_Seek(JNIEnv *env, jobject thiz, jstring uuid, jint target)
{
    PLT_MediaController *dmc = (PLT_MediaController*)env->GetIntField(thiz, fields.mNativeInstance);
    if( dmc == NULL )
    {
        LOGE("DMC_Seek, error on get dmc");
        return;
    }
    PLT_DeviceDataReference dev = uuid2DeviceData(env, dmc, uuid);   
    NPT_String uint("REL_TIME");
    NPT_TimeStamp timestamp;
    LOGD("DMC_Seek, 1 target %d", target);
    timestamp.SetMillis(target);
    NPT_String _target = PLT_Didl::FormatTimeStamp(timestamp.ToSeconds());
    // LOGD("DMC_Seek, 2 target %s", _target.GetChars() );
    dmc->Seek(dev, 0, uint, _target, NULL);
}

#define VIDEO_TYPE 1
#define AUDIO_TYPE 2
#define PHOTO_TYPE 3
static NPT_String BuildDIDLFromUri(JNIEnv *env, const char* uri, int type)
{
    PLT_MediaObject* object = new PLT_MediaItem();
    PLT_MediaItemResource resource;

    if(type == VIDEO_TYPE)
        object->m_ObjectClass.type = "object.item.videoItem";
    else if(type == AUDIO_TYPE)
        object->m_ObjectClass.type = "object.item.audioItem.musicTrack";
    else if(type == PHOTO_TYPE)
        object->m_ObjectClass.type = "object.item.imageItem.photo";
    else
        object->m_ObjectClass.type = "object.item";
    object->m_Title = NPT_FilePath::BaseName(uri, false);
    // resource.m_ProtocolInfo = PLT_ProtocolInfo::GetProtocolInfo(uri, true, NULL);
    resource.m_ProtocolInfo = PLT_ProtocolInfo::GetProtocolInfoFromMimeType("video/mp4", true, NULL);
    resource.m_Size = 0;
    resource.m_Uri = uri;
    object->m_Resources.Add(resource);
    object->m_ParentID = "0";   
    object->m_ObjectID = uri;

    NPT_String ret;    
    object->ToDidl(PLT_FILTER_MASK_ALL, ret);
    // object->ToDidl(0, ret);
    ret = didl_header + ret + didl_footer;
    LOGD("DIDL : %s", ret.GetChars() );
    return ret;
}
static void DMC_SetAVTransportURI(JNIEnv *env, jobject thiz, jstring uuid, jstring uri, jint type)
{
    PLT_MediaController *dmc = (PLT_MediaController*)env->GetIntField(thiz, fields.mNativeInstance);
    if( dmc == NULL )
    {
        LOGE("DMC_SetAVTransportURI, error on get dmc");
        return;
    }
    PLT_DeviceDataReference dev = uuid2DeviceData(env, dmc, uuid);      
    const char *_uri = env->GetStringUTFChars(uri, NULL);
    NPT_String didl = BuildDIDLFromUri(env, _uri, type);
    LOGD("DMC_SetAVTransportURI, uri %s, type %d", _uri, type);
    dmc->SetAVTransportURI(dev, 0, _uri, didl.GetChars(), NULL);  
    env->ReleaseStringUTFChars(uri, _uri);
}

static void DMC_Stop(JNIEnv *env, jobject thiz, jstring uuid)
{
    LOGD("DMC_Stop");
    PLT_MediaController *dmc = (PLT_MediaController*)env->GetIntField(thiz, fields.mNativeInstance);
    if( dmc == NULL )
    {
        LOGE("DMC_Stop, error on get dmc");
        return;
    }
    PLT_DeviceDataReference dev = uuid2DeviceData(env, dmc, uuid);      
    dmc->Stop(dev, 0, NULL);
    dmc->Stop(dev, 0, NULL);
}

static void DMC_SetMute(JNIEnv *env, jobject thiz, jstring uuid, jboolean mute)
{
    PLT_MediaController *dmc = (PLT_MediaController*)env->GetIntField(thiz, fields.mNativeInstance);
    if( dmc == NULL )
    {
        LOGE("DMC_SetMute, error on get dmc");
        return;
    }
    PLT_DeviceDataReference dev = uuid2DeviceData(env, dmc, uuid);      
    dmc->SetMute(dev, 0, "Master", mute, NULL);
}

static void DMC_GetMute(JNIEnv *env, jobject thiz, jstring uuid)
{
    PLT_MediaController *dmc = (PLT_MediaController*)env->GetIntField(thiz, fields.mNativeInstance);
    if( dmc == NULL )
    {
        LOGE("DMC_GetMute, error on get dmc");
        return;
    }
    PLT_DeviceDataReference dev = uuid2DeviceData(env, dmc, uuid);      
    dmc->GetMute(dev, 0, "Master", NULL);
}

static void DMC_SetVolume(JNIEnv *env, jobject thiz, jstring uuid, jint volume)
{
    LOGD("DMC_SetVolume %d", volume);
    PLT_MediaController *dmc = (PLT_MediaController*)env->GetIntField(thiz, fields.mNativeInstance);
    if( dmc == NULL )
    {
        LOGE("DMC_SetVolume, error on get dmc");
        return;
    }
    PLT_DeviceDataReference dev = uuid2DeviceData(env, dmc, uuid);      
    dmc->SetVolume(dev, 0, "Master", volume, NULL);
}

static void DMC_GetVolume(JNIEnv *env, jobject thiz, jstring uuid)
{
    PLT_MediaController *dmc = (PLT_MediaController*)env->GetIntField(thiz, fields.mNativeInstance);
    if( dmc == NULL )
    {
        LOGE("DMC_GetVolume, error on get dmc");
        return;
    }
    PLT_DeviceDataReference dev = uuid2DeviceData(env, dmc, uuid);   
    dmc->GetVolume(dev, 0, "Master", NULL);
}


static JNINativeMethod gMethods[] = 
{
	{"nativeInit"              ,"()I",
	        (void*)DMC_init},
	{"nativeFinalize"          ,"()V",
	        (void*)DMC_finalize},
    {"nativeSearch"             ,"()V",
            (void*)DMC_Search},

    //AVT
    {"nativeGetMediaInfo"              ,"(Ljava/lang/String;)I",
            (void*)DMC_GetMediaInfo},
    {"nativeGetPositionInfo"              ,"(Ljava/lang/String;)I",
            (void*)DMC_GetPositionInfo},
    {"nativeGetTransportInfo"              ,"(Ljava/lang/String;)I",
            (void*)DMC_GetTransportInfo},
    {"nativePause"              ,"(Ljava/lang/String;)I",
            (void*)DMC_Pause},
    {"nativePlay"              ,"(Ljava/lang/String;)I",
            (void*)DMC_Play},
    {"nativeSeek"              ,"(Ljava/lang/String;I)I",
            (void*)DMC_Seek},
    {"nativeSetAVTransportURI"              ,"(Ljava/lang/String;Ljava/lang/String;I)I",
            (void*)DMC_SetAVTransportURI},
    {"nativeStop"              ,"(Ljava/lang/String;)I",
            (void*)DMC_Stop},
    //RC
    {"nativeSetMute"              ,"(Ljava/lang/String;Z)I",
            (void*)DMC_SetMute},
    {"nativeGetMute"              ,"(Ljava/lang/String;)I",
            (void*)DMC_GetMute},
    {"nativeSetVolume"              ,"(Ljava/lang/String;I)I",
            (void*)DMC_SetVolume},
    {"nativeGetVolume"              ,"(Ljava/lang/String;)I",
            (void*)DMC_GetVolume},
};

int register_DMC(JNIEnv *env)
{
	return jniRegisterNativeMethods(env, kClassPathName, gMethods , NELEM(gMethods));
}

}