LOCAL_PATH:= $(call my-dir)

LS_CPP=$(subst $(1)/,,$(wildcard $(1)/$(2)/*.cpp))
LS_C=$(subst $(1)/,,$(wildcard $(1)/$(2)/*.c))

############################ Static lib Neptune/zlib
include $(CLEAR_VARS)

LOCAL_CFLAGS += -Os -DNDEBUG

ZLIB_C_INCLUDES := $(LOCAL_PATH)/Neptune/ThirdParty/zlib-1.2.3

ZLIB_SRC_FILES := $(call LS_C,$(LOCAL_PATH),Neptune/ThirdParty/zlib-1.2.3)

#LOCAL_EXPORT_C_INCLUDES := $(ZLIB_C_INCLUDES)
LOCAL_C_INCLUDES := $(ZLIB_C_INCLUDES)
LOCAL_SRC_FILES := $(ZLIB_SRC_FILES)

LOCAL_MODULE := libzlib

include $(BUILD_STATIC_LIBRARY)

############################ Static lib Neptune/axTLS
include $(CLEAR_VARS)

LOCAL_CFLAGS += -Os -DNDEBUG

AXTLS_C_INCLUDES := $(LOCAL_PATH)/Neptune/ThirdParty/axTLS/config/Generic \
	 $(LOCAL_PATH)/Neptune/ThirdParty/axTLS/crypto \
	 $(LOCAL_PATH)/Neptune/ThirdParty/axTLS/ssl

AXTLS_SRC_FILES := $(call LS_C,$(LOCAL_PATH),Neptune/ThirdParty/axTLS/crypto) \
	 $(call LS_C,$(LOCAL_PATH),Neptune/ThirdParty/axTLS/ssl)

LOCAL_C_INCLUDES := $(AXTLS_C_INCLUDES)
LOCAL_SRC_FILES := $(AXTLS_SRC_FILES)
LOCAL_MODULE := libaxTLS

include $(BUILD_STATIC_LIBRARY)

############################ Static lib Neptune
include $(CLEAR_VARS)

LOCAL_CFLAGS += -DNPT_CONFIG_HAVE_SYSTEM_LOG_CONFIG
LOCAL_CFLAGS += -DNPT_CONFIG_ENABLE_LOGGING
LOCAL_CFLAGS += -Os -DNDEBUG

NEPTUNE_C_INCLUDES := $(LOCAL_PATH)/Neptune/Source/Core \
		 $(AXTLS_C_INCLUDES) \
		 $(ZLIB_C_INCLUDES)
		 
NEPTUNE_SYS_SRC_FILES := $(call LS_CPP,$(LOCAL_PATH),Neptune/Source/System/Android) \
		 $(call LS_CPP,$(LOCAL_PATH),Neptune/Source/System/Posix) \
		 $(call LS_CPP,$(LOCAL_PATH),Neptune/Source/System/Bsd) \
		 Neptune/Source/System/StdC/NptStdcEnvironment.cpp \
		 Neptune/Source/System/StdC/NptStdcFile.cpp \
		 Neptune/Source/System/Null/NptNullSerialPort.cpp

NEPTUNE_SRC_FILES := $(NEPTUNE_SYS_SRC_FILES) \
	 	 $(call LS_CPP,$(LOCAL_PATH),Neptune/Source/Core) \
	 	 $(call LS_CPP,$(LOCAL_PATH),Neptune/Source/Data/TLS/Base) \
	 	 $(call LS_CPP,$(LOCAL_PATH),Neptune/Source/Data/TLS/Extended) \
	 	 $(call LS_CPP,$(LOCAL_PATH),Neptune/Source/Data/TLS) 
	 	 
LOCAL_C_INCLUDES := $(NEPTUNE_C_INCLUDES)
LOCAL_SRC_FILES := $(NEPTUNE_SRC_FILES)

LOCAL_MODULE := libneptune
LOCAL_STATIC_LIBRARIES := libaxTLS libzlib

include $(BUILD_STATIC_LIBRARY)

############################ Static lib Platinum
include $(CLEAR_VARS)

LOCAL_CFLAGS += -DNPT_CONFIG_ENABLE_LOGGING
LOCAL_CFLAGS += -Os -DNDEBUG
LOCAL_CFLAGS += -DPLT_HTTP_DEFAULT_USER_AGENT="\"UPnP/1.0 DLNADOC/1.50 Allwinnertech/0.1.0\""
LOCAL_CFLAGS += -DPLT_HTTP_DEFAULT_SERVER="\"UPnP/1.0 DLNADOC/1.50 Allwinnertech/0.1.0\""
LOCAL_CFLAGS += -DDLNA_SPEC_STRICT


PLATINUM_C_INCLUDES :=  $(LOCAL_PATH)/Platinum/Source/Core \
		 $(LOCAL_PATH)/Platinum/Source/Platinum \
		 $(LOCAL_PATH)/Platinum/Source/Devices/MediaConnect \
		 $(LOCAL_PATH)/Platinum/Source/Devices/MediaRenderer \
		 $(LOCAL_PATH)/Platinum/Source/Devices/MediaServer \
		 $(LOCAL_PATH)/Platinum/Source/Extras/ \
		 $(NEPTUNE_C_INCLUDES)

PLATINUM_SRC_FILES := $(call LS_CPP,$(LOCAL_PATH),Platinum/Source/Core) \
	 	 $(call LS_CPP,$(LOCAL_PATH),Platinum/Source/Devices/MediaConnect) \
	 	 $(call LS_CPP,$(LOCAL_PATH),Platinum/Source/Devices/MediaRenderer) \
	 	 $(call LS_CPP,$(LOCAL_PATH),Platinum/Source/Devices/MediaServer) \
	 	 $(call LS_CPP,$(LOCAL_PATH),Platinum/Source/Extras) 

LOCAL_C_INCLUDES := $(PLATINUM_C_INCLUDES)
LOCAL_SRC_FILES := $(PLATINUM_SRC_FILES)

LOCAL_MODULE := libplatinum
LOCAL_STATIC_LIBRARIES := libneptune

include $(BUILD_STATIC_LIBRARY)