package com.softwinner.fireplayer.dlna;

import java.util.ArrayList;
import java.util.List;

import android.R.integer;
import android.util.Log;

public class DMC
{
	static 
	{
		System.loadLibrary("dlna");
	}
	
	public static final String TAG = "DMC";

	public static final int DLNA_STOPPED = 0;
	public static final int DLNA_PAUSED_PLAYBACK = 1;
	public static final int DLNA_PLAYING = 2;
	public static final int DLNA_TRANSITIONING = 3;
	public static final int DLNA_NO_MEDIA_PRESENT = 4;
	
	public static final int VIDEO_TYPE = 1;
	public static final int AUDIO_TYPE = 2;
	public static final int PHOTO_TYPE = 3;

	private int mNativeInstance;
	
	// private List<DMR> mDMRs;
	public List<DMRInDMC> mDMRs = new ArrayList<DMRInDMC>();
	public int mCurrentIndex = -1;	
		
	public class DMRInDMC
	{
		public String uuid;
		public String name;
		
		DMRInDMC(String u, String n)
		{
			uuid = u;
			name = n;
		}
	}
	
	public interface OnDMCListener
	{
//		public void onMRAdded(String uuid, String name);
//		public void onMRRemoved(String uuid);
		public void onMRChanged(List<DMRInDMC> DMRs);
		public void onMRTransportStateChanged(int state);
		public void onMRVolumeChanged(int volume);

		//AVTransport
		// public void onGetCurrentTransportActionsResult();
		// public void onGetDeviceCapabilitiesResult();
		public void onGetMediaInfoResult(int dura, String uri, String metadata);
		public void onGetPositionInfoResult(int dura, int pos);
		public void onGetTransportInfoResult(int transport_state);
		// public void onGetTransportSettingsResult();
		// public void onNextResult();
		// public void onPauseResult();
		// public void onPlayResult();
		// public void onPreviousResult();
		 public void onSeekResult();
		 public void onSetAVTransportURIResult();
		// public void onSetPlayModeResult();
		// public void onStopResult();
		//ConnectionManager
		// public void onGetCurrentConnectionIDsResult();
		// public void onGetCurrentConnectionInfoResult();
		// public void onGetProtocolInfoResult();
		//RenderingControl
		// public void onSetMuteResult();
		public void onGetMuteResult(boolean mute);
		// public void onSetVolumeResult();
		public void onGetVolumeResult(int volume);
	}

	private OnDMCListener mListener;
	
	public DMC()
	{
		mNativeInstance = nativeInit();
	}

	@Override
	public void finalize() throws Throwable 
	{
		super.finalize();
		nativeFinalize();
	}

	public void SetOnDMCListerner(OnDMCListener l)
	{
		mListener = l;
	}

	private void onMRAdded(String uuid, String name)
	{
		mDMRs.add( new DMRInDMC(uuid, name) );
		if( mCurrentIndex == -1 )
			mCurrentIndex = 0;
		if( mListener != null )
			mListener.onMRChanged(mDMRs);
	}

	private void onMRRemoved(String uuid)
	{
		int i=0;
		for( DMRInDMC r : mDMRs )
		{
			if( r.uuid.equals(uuid) )
			{
				mDMRs.remove(r);
				if(i == mCurrentIndex )
					mCurrentIndex = -1;
				break;
			}
			i++;
		}
		if( mListener != null )
			mListener.onMRChanged(mDMRs);
	}

	private void onMRTransportStateChanged(int state)
	{
		if( mListener != null )
			mListener.onMRTransportStateChanged(state);
	}

	private void onMRVolumeChanged(int volume) {
		if( mListener != null )
			mListener.onMRVolumeChanged(volume);
	}

	private void onGetMediaInfoResult(int dura, String uri, String metadata)
	{
		if( mListener != null )
			mListener.onGetMediaInfoResult(dura, uri, metadata);
	}

	private void onGetPositionInfoResult(int dura, int pos)  // unit : s
	{
		if( mListener != null )
			mListener.onGetPositionInfoResult(dura, pos);
	}

	private void onGetTransportInfoResult(int transport_state)
	{
		if( mListener != null )
			mListener.onGetTransportInfoResult(transport_state);
	}
	
	private void onSeekResult()
	{
		if( mListener != null )
			mListener.onSeekResult();
	}
	
	private void onSetAVTransportURIResult()
	{
		if( mListener != null )
			mListener.onSetAVTransportURIResult();
	}

	private void onGetMuteResult(boolean mute)
	{
		if( mListener != null )
			mListener.onGetMuteResult(mute);
	}

	private void onGetVolumeResult(int volume)
	{
		if( mListener != null )
			mListener.onGetVolumeResult(volume);
	}

	// AVTransport
    // int GetCurrentTransportActions(String dev);
    // int GetDeviceCapabilities(String dev);
    public int GetMediaInfo()
    {
    	if( 0<=mCurrentIndex && mCurrentIndex<mDMRs.size() )
    		return nativeGetMediaInfo( mDMRs.get(mCurrentIndex).uuid );
    	else 
			return 0;
    }
    public int GetPositionInfo()
    {
    	if( 0<=mCurrentIndex && mCurrentIndex<mDMRs.size() )
    		return nativeGetPositionInfo( mDMRs.get(mCurrentIndex).uuid );
    	else  
			return 0;
    }
    public int GetTransportInfo()
    {
    	if( 0<=mCurrentIndex && mCurrentIndex<mDMRs.size() )
    		return nativeGetTransportInfo( mDMRs.get(mCurrentIndex).uuid );
    	else
    		return 0;
    }
    // int GetTransportSettings(String dev);
    // int Next(String dev);
    public int Pause()
    {
    	if( 0<=mCurrentIndex && mCurrentIndex<mDMRs.size() )
    		return nativePause( mDMRs.get(mCurrentIndex).uuid );
    	else
    		return 0;
    }
    public int Play()
    {
    	if( 0<=mCurrentIndex && mCurrentIndex<mDMRs.size() )
    		return nativePlay( mDMRs.get(mCurrentIndex).uuid );
    	else
    		return 0;
    }
    // int Previous(String dev);
    public int Seek(int target)
    {
    	if( 0<=mCurrentIndex && mCurrentIndex<mDMRs.size() )
    		return nativeSeek(mDMRs.get(mCurrentIndex).uuid, target);
    	else
    		return 0;
    }
    public int SetAVTransportURI(String uri, int type)
    {
    	if( 0<=mCurrentIndex && mCurrentIndex<mDMRs.size() )
    		return nativeSetAVTransportURI(mDMRs.get(mCurrentIndex).uuid, uri, type);
    	else
    		return 0;
    }
    // int SetNextAVTransportURI(String dev, String next_uri, String next_metadata);
    // int SetPlayMode(String  dev, String new_play_mode);
    public int Stop()
    {
    	if( 0<=mCurrentIndex && mCurrentIndex<mDMRs.size() )
    		return nativeStop( mDMRs.get(mCurrentIndex).uuid );
    	else
    		return 0;
    }
    // ConnectionManager
    // int GetCurrentConnectionIDs(String dev);
    // int GetCurrentConnectionInfo(String dev, NPT_UInt32 connection_id);
    // int GetProtocolInfo(String dev);
    // RenderingControl
    public int SetMute(boolean mute)
    {
    	if( 0<=mCurrentIndex && mCurrentIndex<mDMRs.size() )
    		return nativeSetMute(mDMRs.get(mCurrentIndex).uuid, mute);
    	else 
			return 0;
    }
    public int GetMute()
    {
    	if( 0<=mCurrentIndex && mCurrentIndex<mDMRs.size() )
    		return nativeGetMute( mDMRs.get(mCurrentIndex).uuid );
    	else
    		return 0;
    }
	public int SetVolume(int volume)
	{
		if( 0<=mCurrentIndex && mCurrentIndex<mDMRs.size() )
			return nativeSetVolume(mDMRs.get(mCurrentIndex).uuid, volume);
		else 
			return 0;
	}
	public int GetVolume()
	{
		if( 0<=mCurrentIndex && mCurrentIndex<mDMRs.size() )
			return nativeGetVolume( mDMRs.get(mCurrentIndex).uuid );
		else
			return 0;
	}
	
	public void ClearDMRs() 
	{
		mDMRs.clear();
	}
	public void Search()
	{
		nativeSearch();
	}

	public void SetDMR(int index)
	{
		if( index>=0 && index<mDMRs.size() )
			mCurrentIndex = index;
	}
	
	public List<DMRInDMC> GetDMRsInDMC() {
		return mDMRs;
	}
	
	private native int nativeInit();
	private native void nativeFinalize();

	private native void nativeSearch();
	//AVT
	private native int nativeGetMediaInfo(String dev);
	private native int nativeGetPositionInfo(String dev);
	private native int nativeGetTransportInfo(String dev);
	private native int nativePause(String dev);
	private native int nativePlay(String  dev);
	private native int nativeSeek(String  dev, int target);
	private native int nativeSetAVTransportURI(String dev, String uri, int type);
	private native int nativeStop(String dev);
	//RC
	private native int nativeSetMute(String dev, boolean mute);
	private native int nativeGetMute(String dev);
	private native int nativeSetVolume(String dev, int volume);
	private native int nativeGetVolume(String dev);
}