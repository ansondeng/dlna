package com.softwinner.fireplayer.dlna;

public class DLNAManager 
{
	static 
	{
		System.loadLibrary("dlna");
	}

	private int mNativeInstance;
	private static DLNAManager mInstance = null;
	
	public DMC mDMC = null;
	public DMS mDMS = null;

	public static DLNAManager getInstance()
	{
		if( mInstance == null ) {
			mInstance = new DLNAManager();
			return mInstance;
		}
		return mInstance;
	}
	
	private DLNAManager()
	{
		mNativeInstance = nativeInit();
	}

	@Override
	public void finalize() throws Throwable 
	{	
		super.finalize();
		nativeFinalize();
		mInstance = null;
	}

	public void AddDMS(DMS dev)
	{
		mDMS = dev;
		nativeAddDMS(dev);
	}

	public void RemoveDMS(DMS dev)
	{
		nativeRemoveDMS(dev);
		mDMS = null;
	}

	public void AddDMC(DMC cp)
	{
		mDMC = cp;
		nativeAddDMC(cp);
	}

	public void RemoveDMC(DMC cp)
	{
		nativeRemoveDMC(cp);
		mDMC = null;
	}

	private native int nativeInit();
	private native void nativeFinalize();

	private native void nativeAddDMS(DMS dev);
	private native void nativeRemoveDMS(DMS dev);
	// private native void nativeAddDMR(DMR dev);
	// private native void nativeRemoveDMR(DMR dev);

	private native void nativeAddDMC(DMC cp);
	private native void nativeRemoveDMC(DMC cp);
	// private native void nativeAddDMB(DMB cp);
	// private native void nativeRemoveDMB(DMB cp);	
}