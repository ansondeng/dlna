package com.softwinner.fireplayer.dlna;

public class DMS
{
	static 
	{
		System.loadLibrary("dlna");
	}

	private int mNativeInstance;

	public DMS(String root, String name)
	{
		mNativeInstance = nativeInit(root, name);
	}

	@Override
	public void finalize() throws Throwable 
	{
		super.finalize();
		nativeFinalize();
	}

	public String GetURLFromPath(String path)
	{
		return nativeGetURLFromPath(path);
	}

	private native int nativeInit(String root, String name);
	private native void nativeFinalize();
	private native String nativeGetURLFromPath(String path);
}